%global project_name FcitxQt5

Name:           fcitx-qt5
Version:        1.2.7
Release:        1
Summary:        Fcitx IM module for Qt5
License:        GPL-2.0-or-later and BSD-3-Clause
URL:            https://github.com/fcitx/fcitx-qt5
Source0:        https://download.fcitx-im.org/%{name}/%{name}-%{version}.tar.xz

BuildRequires:  cmake, fcitx-devel, qt5-qtbase-devel, libxkbcommon-devel, extra-cmake-modules, gettext-devel qt5-qtbase-private-devel
%{?_qt5:Requires: %{_qt5}%{?_isa} = %{_qt5_version}}
%global __provides_exclude_from ^(%{_qt5_plugindir}/platforminputcontexts/libfcitxplatforminputcontextplugin\\.so|%{_libdir}/fcitx/qt/libfcitx-quickphrase-editor5\\.so)$

%description
This package provides Fcitx Qt5 input context.

%package devel
Summary:        Development files for fcitx-qt5
Requires:       %{name}%{?_isa} = %{version}-%{release}

%description devel
The %{name}-devel package contains libraries and header files necessary for
developing programs using fcitx-qt5 libraries.

%prep
%setup -q

%build
%cmake
%cmake_build

%install
%cmake_install
%find_lang %{name}

%files -f %{name}.lang
%doc README
%license COPYING COPYING.BSD
%{_libdir}/fcitx/libexec/%{name}-gui-wrapper
%{_libdir}/lib%{project_name}*.so.*
%{_libdir}/fcitx/qt/
%{_qt5_plugindir}/platforminputcontexts/libfcitxplatforminputcontextplugin.so

%files devel
%{_includedir}/%{project_name}
%{_libdir}/lib%{project_name}*.so
%{_libdir}/cmake/*

%changelog
* Thu Nov 07 2024 Funda Wang <fundawang@yeah.net> - 1.2.7-1
- update to 1.2.7

* Tue Dec 22 2020 weidong <weidong@uniontech.com> - 1.2.4-1
- Initial package
